﻿
var myApp = angular.module("myApp", ["ui.router", "orders", "agents"]).config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
    $locationProvider.html5Mode(true);
    $stateProvider
        .state("home", {
            url: "/",
            templateUrl: "views/home/home.html",
            controller: function () {
            }
        })
    ;
}).run(function ($rootScope, $state) {
    $rootScope.$state = $state;
});

angular.module("orders", ["ui.router"]).config(function ($stateProvider, $urlRouterProvider) {    
    $stateProvider
        .state("orders", {
            url: "/orders",
            templateUrl: "views/orders/home.html",
            controller: function () {
            }
        })
        .state("orders.open", {
            url: "/open",
            templateUrl: "views/orders/open.html",
            controller: function () {
            }
        })
        .state("orders.new", {
            url: "/new",
            templateUrl: "views/orders/new.html",
            controller: function ($scope) {
                $scope.submit = function () {
                }
            }
        })
        .state("orders.closed", {
            url: "/closed",
            templateUrl: "views/orders/closed.html",
            controller: function () {
            }
        })
        .state("orders.reports", {
            url: "/reports",
            templateUrl: "views/orders/reports.html",
            controller: function () {
            }
        })
    ;
});

angular.module("agents", ["ui.router"]).config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state("agents", {
            url: "/agents",
            templateUrl: "views/agents/home.html",
            controller: function () {
            }
        })
    ;
});
